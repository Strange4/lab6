module main {
    requires javafx.controls;
    requires javafx.fxml;


    opens main.gui to javafx.fxml;
    exports main.gui;
    exports main.game;
}