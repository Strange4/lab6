package main.game;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random rand;

    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.rand = new Random();
    }

    public int getLosses() {
        return losses;
    }

    public int getTies() {
        return ties;
    }

    public int getWins() {
        return wins;
    }

    public String playRound(String player) {
        String choiceAI;
        switch (this.rand.nextInt((2) + 1)) {
            case 0:
                choiceAI = "scissors";
                break;
            case 1:
                choiceAI = "rock";
                break;
            case 2:
                choiceAI = "paper";
                break;
            default:
                choiceAI = "not";
                break;
        }
        if (choiceAI.equals("not")) {
            this.wins += 1;
            return "You got lucky! A fatal error occurred and the computer could not choose what to play! You win this round!";
        }
        String choicesTogether = player + "-" + choiceAI;
        if (player.equals(choiceAI)) {
            this.ties += 1;
            return "It's a tie!";
        } else if (choicesTogether.equals("rock-paper") || choicesTogether.equals("scissors-rock") || choicesTogether.equals("paper-scissors")) {
            this.losses += 1;
            return "The AI chose " + choiceAI + ", you chose " + player + "!";
        } else {
            this.wins += 1;
            return "You beat the computer! You win!";
        }
    }
}
