package main.gui;

import main.game.RpsGame;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class RpsApplication extends Application {
    private RpsGame game = new RpsGame();

    @Override
    public void start(Stage stage) {
        Group root = new Group();
        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.BLACK);
        HBox hb1 = new HBox();
        HBox hb2 = new HBox();

        InfoWidget infoWidget = new InfoWidget();

        hb1.getChildren().add(new ButtonChoices(infoWidget, this.game));
        hb2.getChildren().addAll(infoWidget);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(hb1, hb2);
        root.getChildren().add(vBox);
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}