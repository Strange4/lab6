package main.gui;

import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class InfoWidget extends HBox {
    private final TextField info;
    private final TextField winsTf;
    private final TextField lossTf;
    private final TextField tiesTf;
    public InfoWidget(){
        this.info = new TextField("Welcome!");
        this.winsTf = new TextField("wins: 0");
        this.lossTf = new TextField("losses: 0");
        this.tiesTf = new TextField("ties: 0");
        info.setPrefWidth(200);

        this.getChildren().addAll(info, winsTf, lossTf, tiesTf);
    }

    public TextField getInfo() {
        return info;
    }

    public TextField getWinsTf() {
        return winsTf;
    }

    public TextField getLossTf() {
        return lossTf;
    }

    public TextField getTiesTf() {
        return tiesTf;
    }
}
