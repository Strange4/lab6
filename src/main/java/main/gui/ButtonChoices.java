package main.gui;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import main.game.RpsGame;

public class ButtonChoices extends HBox {
    private final Button rockBtn;
    private final Button paperBtn;
    private final Button scissorBtn;

    public ButtonChoices(InfoWidget textFields, RpsGame game){
        this.rockBtn = new Button("rock");
        this.paperBtn = new Button("paper");
        this.scissorBtn = new Button("scissors");
        this.getChildren().addAll(rockBtn, paperBtn, scissorBtn);
        setOnActionEvents(textFields, game);
    }

    private void setOnActionEvents(InfoWidget textFields, RpsGame game){
        this.rockBtn.setOnAction(new RpsChoice(textFields, "rock", game));
        this.paperBtn.setOnAction(new RpsChoice(textFields, "paper", game));
        this.scissorBtn.setOnAction(new RpsChoice(textFields, "scissors", game));
    }
}
