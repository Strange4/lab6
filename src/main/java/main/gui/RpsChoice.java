package main.gui;

import main.game.RpsGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RpsChoice implements EventHandler<ActionEvent> {
    private final InfoWidget infoWidget;
    private final RpsGame game;
    private final String playerChoice;

    public RpsChoice(InfoWidget infoWidget, String playerChoice, RpsGame game){
        this.infoWidget = infoWidget;
        this.game = game;
        this.playerChoice = playerChoice;
    }

    @Override
    public void handle(ActionEvent actionEvent) {

        String result = this.game.playRound(this.playerChoice);
        this.infoWidget.getInfo().setText(result);
        this.infoWidget.getLossTf().setText("losses: "+this.game.getLosses());
        this.infoWidget.getWinsTf().setText("wins: "+this.game.getWins());
        this.infoWidget.getTiesTf().setText("ties: "+this.game.getTies());
    }
}
